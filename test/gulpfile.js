const { src, dest, watch, series} = require('gulp');
const template = require('gulp-template-html');
const sass = require('gulp-sass')(require('sass'));
const prefix = require('gulp-autoprefixer');
const minify = require('gulp-clean-css');
const fileinclude = require('gulp-file-include');

//group html
function groupHtml() {
    return src("./src/pages/*.html")
        .pipe(template("./src/templates/template.html"))
        .pipe(dest("./dist"));
};

//compile, prefix, and min scss
function compilescss() {
    return src('src/styles/sass/*.scss') // change to your source directory
        .pipe(sass())
        .pipe(prefix('last 2 versions'))
        .pipe(minify())
        .pipe(dest('dist/css')) // change to your final/public directory
};

function include(){
    return src(['src/index.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(dest('src/pages'));
};


//watchtask
function watchTask(){
    watch('src/build/*.html', include);
    watch('src/pages/*.html', groupHtml); // change to your source directory
    watch('src/styles/sass/*.scss', compilescss); // change to your source directory
}

// Default Gulp task 
exports.default = series(
    include,
    groupHtml,
    compilescss,
    watchTask
);