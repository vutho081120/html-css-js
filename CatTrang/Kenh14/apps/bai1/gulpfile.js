const { src, dest, watch, series} = require('gulp');
const template = require('gulp-template-html');
const sass = require('gulp-sass')(require('sass'));
const prefix = require('gulp-autoprefixer');
const minify = require('gulp-clean-css');
const browsersync = require('browser-sync').create();

//browser-sync
function browsersyncServe(cb){
    browsersync.init({
        server: {
            baseDir: ['./dist/html/', './dist/', './']
        }    
    });
    cb();
}
function browsersyncReload(cb){
    browsersync.reload();
    cb();
}

//group html
function groupHtml() {
    return src("./src/pages/*.html")
        .pipe(template("./src/templates/template.html"))
        .pipe(dest("./dist/html"));
}

//compile, prefix, and min scss
function compilescss() {
    return src('./src/styles/sass/*.scss') // change to your source directory
        .pipe(sass())
        .pipe(prefix('last 2 versions'))
        //.pipe(minify())
        .pipe(dest("./dist/css")) // change to your final/public directory
}

//watchtask
function watchTask(){
    // watch('./src/pages/*.html', groupHtml); // change to your source directory
    // watch('./src/styles/sass/*.scss', compilescss); // change to your source directory
    watch(['./src/pages/*.html', './src/styles/sass/*.scss'], series(groupHtml, compilescss, browsersyncReload));
}

// Default Gulp task 
exports.default = series(
    groupHtml,
    compilescss,
    browsersyncServe,
    watchTask
);