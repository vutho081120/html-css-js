var temp = 0;
function next() {
    if (temp == 0) {
        temp += -575;
        document.getElementById("gallery1").style.transform  = 'translateX('+temp+'px)';
        document.getElementById("btn-back").style.backgroundColor = '#FB6C27';
        document.getElementById("dot1").style.backgroundColor = '#C4C4C4';
        document.getElementById("dot2").style.backgroundColor = '#FB512A';
    }
    else if (temp == -575) {
        temp = -740;
        document.getElementById("gallery1").style.transform  = 'translateX('+temp+'px)';
        document.getElementById("btn-next").style.backgroundColor = '#5A5A5A';
        document.getElementById("dot2").style.backgroundColor = '#C4C4C4';
        document.getElementById("dot3").style.backgroundColor = '#FB512A';
    }
}

function back() {
    temp = 0;
    document.getElementById("gallery1").style.transform  = 'translateX('+temp+'px)';
    document.getElementById("btn-back").style.backgroundColor = '#5A5A5A';
    document.getElementById("btn-next").style.backgroundColor = '#FB6C27';
    document.getElementById("dot1").style.backgroundColor = '#FB512A';
    document.getElementById("dot3").style.backgroundColor = '#C4C4C4';
}

function dot1() {
    temp = 0;
    document.getElementById("gallery1").style.transform  = 'translateX('+temp+'px)';
    document.getElementById("btn-back").style.backgroundColor = '#5A5A5A';
    document.getElementById("btn-next").style.backgroundColor = '#FB6C27';
    document.getElementById("dot1").style.backgroundColor = '#FB512A';
    document.getElementById("dot2").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot3").style.backgroundColor = '#C4C4C4';
}

function dot2() {
    temp = -575;
    document.getElementById("gallery1").style.transform  = 'translateX('+temp+'px)';
    document.getElementById("btn-back").style.backgroundColor = '#FB6C27';
    document.getElementById("dot1").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot2").style.backgroundColor = '#FB512A';
    document.getElementById("dot3").style.backgroundColor = '#C4C4C4';
}

function dot3() {
    temp = -740;
    document.getElementById("gallery1").style.transform  = 'translateX('+temp+'px)';
    document.getElementById("btn-next").style.backgroundColor = '#5A5A5A';
    document.getElementById("dot1").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot2").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot3").style.backgroundColor = '#FB512A';
}