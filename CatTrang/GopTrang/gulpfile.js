const { src, dest, watch, series} = require('gulp');
const template = require('gulp-template-html');
const sass = require('gulp-sass')(require('sass'));
const prefix = require('gulp-autoprefixer');
const minify = require('gulp-clean-css');
const fileinclude = require('gulp-file-include');
const terser = require('gulp-terser');
const browserSync = require('browser-sync').create();

//browser-sync
function browsersyncServe(cb){
    browserSync.init({
        server: {
            baseDir: ['./apps/dist/html/', './apps/dist/', './apps/']
        }
    });
    cb();
}
function browsersyncReload(cb){
    browserSync.reload();
    cb();
}

//js
function jsmin() {
    return src("./apps/src/styles/js/**/*.js")
        .pipe(terser())
        .pipe(dest("./apps/dist/js"))
}

//group html
function groupHtml() {
    return src("././apps/src/pages/*.html")
        .pipe(template("././apps/src/templates/template.html"))
        .pipe(dest("./apps/dist/html"))
}

//compile, prefix, and min scss
function compilescss() {
    return src('./apps/src/styles/sass/**/*.scss') // change to your source directory
        .pipe(sass())
        .pipe(prefix('last 2 versions'))
        .pipe(minify())
        .pipe(dest('./apps/dist/css')) // change to your final/public directory
}

//include html
function include(){
    return src(['./apps/src/component/index/index.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(dest('./apps/src/pages'))
}


//watchtask
function watchTask(){
    watch('./apps/src/component/**/*.html', include); // change to your source directory
    watch(['./apps/src/pages/*.html', './apps/src/styles/sass/**/*.scss', './apps/src/styles/js/**/*.js'], series(groupHtml, compilescss, jsmin, browsersyncReload));
}

// Default Gulp task 
exports.default = series(
    include,
    groupHtml,
    compilescss,
    jsmin,
    browsersyncServe,
    watchTask
);