var temp = 0;
function next() {
    if (temp == 0) {
        temp += -575;
        document.getElementById("gallery5").style.transform  = 'translateX('+temp+'px)';
        document.getElementById("btn-back").style.backgroundColor = '#fff';
    }
    else if (temp == -575) {
        temp = -740;
        document.getElementById("gallery5").style.transform  = 'translateX('+temp+'px)';
        document.getElementById("btn-next").style.backgroundColor = '#fff';
    }
}

function back() {
    temp = 0;
    document.getElementById("gallery5").style.transform  = 'translateX('+temp+'px)';
    document.getElementById("btn-back").style.backgroundColor = '#fff';
    document.getElementById("btn-next").style.backgroundColor = '#fff';
}