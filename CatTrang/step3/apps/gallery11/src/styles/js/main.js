var temp = 0;
function next() {
    if (temp == 0) {
        temp += -540;
        document.getElementById("gallery4").style.transform  = 'translateX('+temp+'px)';
        document.getElementById("btn-back").style.backgroundColor = '#FB6C27';
        document.getElementById("dot1").style.backgroundColor = '#C4C4C4';
        document.getElementById("dot2").style.backgroundColor = '#FB512A';
    }
    else if (temp == -540) {
        temp += -540;
        document.getElementById("gallery4").style.transform  = 'translateX('+temp+'px)';
        document.getElementById("dot2").style.backgroundColor = '#C4C4C4';
        document.getElementById("dot3").style.backgroundColor = '#FB512A';
    }
    else if (temp == -540*2) {
        temp += -90;
        document.getElementById("gallery4").style.transform  = 'translateX('+temp+'px)';
        document.getElementById("btn-next").style.backgroundColor = '#5A5A5A';
        document.getElementById("dot3").style.backgroundColor = '#C4C4C4';
        document.getElementById("dot4").style.backgroundColor = '#FB512A';
    }
}

function back() {
    if (temp == -540*2-90) {
        temp = -540;
        document.getElementById("gallery4").style.transform  = 'translateX('+temp+'px)';
        document.getElementById("btn-next").style.backgroundColor = '#FB6C27';
        document.getElementById("dot4").style.backgroundColor = '#C4C4C4';
        document.getElementById("dot2").style.backgroundColor = '#FB512A';
    }
    else if (temp == -540*2) {
        temp += 540;
        document.getElementById("gallery4").style.transform  = 'translateX('+temp+'px)';
        document.getElementById("dot3").style.backgroundColor = '#C4C4C4';
        document.getElementById("dot2").style.backgroundColor = '#FB512A';
    }
    else {
        temp = 0;
        document.getElementById("gallery4").style.transform  = 'translateX('+temp+'px)';
        document.getElementById("btn-back").style.backgroundColor = '#5A5A5A';
        document.getElementById("btn-next").style.backgroundColor = '#FB6C27';
        document.getElementById("dot1").style.backgroundColor = '#FB512A';
        document.getElementById("dot2").style.backgroundColor = '#C4C4C4';
    }
}

function dot1() {
    temp = 0;
    document.getElementById("gallery4").style.transform  = 'translateX('+temp+'px)';
    document.getElementById("btn-back").style.backgroundColor = '#5A5A5A';
    document.getElementById("btn-next").style.backgroundColor = '#FB6C27';
    document.getElementById("dot1").style.backgroundColor = '#FB512A';
    document.getElementById("dot2").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot3").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot4").style.backgroundColor = '#C4C4C4';
}

function dot2() {
    temp = -540;
    document.getElementById("gallery4").style.transform  = 'translateX('+temp+'px)';
    document.getElementById("btn-back").style.backgroundColor = '#FB6C27';
    document.getElementById("dot1").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot2").style.backgroundColor = '#FB512A';
    document.getElementById("dot3").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot4").style.backgroundColor = '#C4C4C4';
}

function dot3() {
    temp = -540*2;
    document.getElementById("gallery4").style.transform  = 'translateX('+temp+'px)';
    document.getElementById("dot1").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot2").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot3").style.backgroundColor = '#FB512A';
    document.getElementById("dot4").style.backgroundColor = '#C4C4C4';
}

function dot4() {
    temp = -540*2-90;
    document.getElementById("gallery4").style.transform  = 'translateX('+temp+'px)';
    document.getElementById("btn-next").style.backgroundColor = '#5A5A5A';
    document.getElementById("dot1").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot2").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot3").style.backgroundColor = '#C4C4C4';
    document.getElementById("dot4").style.backgroundColor = '#FB512A';
}