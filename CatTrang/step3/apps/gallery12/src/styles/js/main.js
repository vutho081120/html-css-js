var temp = 0;
function up() {
    if (temp == -176*2) {
        temp += 176;
        document.getElementById("gallery7").style.transform  = 'translateY('+temp+'px)';
        document.getElementById("page").innerHTML = '2/3';
    }
    else if (temp == -176) {
        temp = 0;
        document.getElementById("gallery7").style.transform  = 'translateY('+temp+'px)';
        document.getElementById("page").innerHTML = '1/3';
    }
}

function down() {
    if (temp == 0) {
        temp += -176;
        document.getElementById("gallery7").style.transform  = 'translateY('+temp+'px)';
        document.getElementById("page").innerHTML = '2/3';
    }
    else if (temp == -176) {
        temp += -176;
        document.getElementById("gallery7").style.transform  = 'translateY('+temp+'px)';
        document.getElementById("page").innerHTML = '3/3';
    }
}