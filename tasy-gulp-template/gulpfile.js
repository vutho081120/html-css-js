var gulp = require('gulp');
 
var template = require('gulp-template-html');

gulp.task("build-template", function() {
    return gulp.src("./src/pages/*.html")
    .pipe(template("./src/template/main.html"))
    .pipe(gulp.dest("./dist"));
});